import dto.*;
import methods.*;

public class Ejercicio9App {
	public static void main(String[] args) {
		Database newDatabase = new Database("UD18Ejercicio9");
		Table Facultad = new Table("UD18Ejercicio9", "Facultad");
		Table Investigadores = new Table("UD18Ejercicio9", "Investigadores");
		Table Equipos = new Table("UD18Ejercicio9", "Equipos");
		Table Reserva = new Table("UD18Ejercicio9", "Reserva");
		openConnection.openConnection();
		createDB.createDB(newDatabase);
		createTable.createTable(Facultad, " ( `Codigo` INT NOT NULL, `Nombre` NVARCHAR(100) NULL, PRIMARY KEY (`Codigo`));");
		createTable.createTable(Investigadores, " (`DNI` VARCHAR(8) NOT NULL, `NomApels` NVARCHAR(255) NULL, `Facultad` INT NULL, PRIMARY KEY (`DNI`), INDEX `Facultad_idx` (`Facultad` ASC) VISIBLE, CONSTRAINT `Facultad` FOREIGN KEY (`Facultad`) REFERENCES `UD18Ejercicio9`.`Facultad` (`Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION);");
		createTable.createTable(Equipos, " (`NumSerie` CHAR(4) NOT NULL, `Nombre` NVARCHAR(100) NULL, `Facultad` INT NULL, PRIMARY KEY (`NumSerie`), INDEX `Facultad_idx` (`Facultad` ASC) VISIBLE, CONSTRAINT `FacultadE` FOREIGN KEY (`Facultad`) REFERENCES `UD18Ejercicio9`.`Facultad` (`Codigo`) ON DELETE NO ACTION ON UPDATE NO ACTION);");
		createTable.createTable(Reserva, " (`DNI` VARCHAR(8) NOT NULL, `NumSerie` CHAR(4) NOT NULL, `Comienzo` DATETIME NULL, `Fin` DATETIME NULL, PRIMARY KEY (`DNI`, `NumSerie`), INDEX `NumSerie_idx` (`NumSerie` ASC) VISIBLE, CONSTRAINT `DNI` FOREIGN KEY (`DNI`) REFERENCES `UD18Ejercicio9`.`Investigadores` (`DNI`) ON DELETE NO ACTION ON UPDATE NO ACTION, CONSTRAINT `NumSerie` FOREIGN KEY (`NumSerie`) REFERENCES `UD18Ejercicio9`.`Equipos` (`NumSerie`) ON DELETE NO ACTION ON UPDATE NO ACTION);");
		insertData.insertData("UD18Ejercicio9", "Facultad", "(Codigo, Nombre) VALUE(1, 'Medicina');");
		insertData.insertData("UD18Ejercicio9", "Facultad", "(Codigo, Nombre) VALUE(2, 'Ingenieria');");
		insertData.insertData("UD18Ejercicio9", "Facultad", "(Codigo, Nombre) VALUE(3, 'Artes');");
		insertData.insertData("UD18Ejercicio9", "Facultad", "(Codigo, Nombre) VALUE(4, 'Lenguas');");
		insertData.insertData("UD18Ejercicio9", "Facultad", "(Codigo, Nombre) VALUE(5, 'Ciencias');");
		insertData.insertData("UD18Ejercicio9", "Investigadores", "(DNI, NomApels, Facultad) VALUE('39182736', 'Pau Abella', 1);");
		insertData.insertData("UD18Ejercicio9", "Investigadores", "(DNI, NomApels, Facultad) VALUE('39165336', 'Sergi Lopez', 2);");
		insertData.insertData("UD18Ejercicio9", "Investigadores", "(DNI, NomApels, Facultad) VALUE('49718236', 'Daniel Maya', 3);");
		insertData.insertData("UD18Ejercicio9", "Investigadores", "(DNI, NomApels, Facultad) VALUE('88122736', 'Alberto Garcia', 4);");
		insertData.insertData("UD18Ejercicio9", "Investigadores", "(DNI, NomApels, Facultad) VALUE('39184867', 'Oscar Trillas', 5);");
		insertData.insertData("UD18Ejercicio9", "Equipos", "(NumSerie, Nombre, Facultad) VALUE('A2E1', 'CovidInvest', 1);");
		insertData.insertData("UD18Ejercicio9", "Equipos", "(NumSerie, Nombre, Facultad) VALUE('UU32', 'ELAInvest', 2);");
		insertData.insertData("UD18Ejercicio9", "Equipos", "(NumSerie, Nombre, Facultad) VALUE('AAE2', 'AlzheimerInvest', 3);");
		insertData.insertData("UD18Ejercicio9", "Equipos", "(NumSerie, Nombre, Facultad) VALUE('OPAR', 'CancerInvest', 4);");
		insertData.insertData("UD18Ejercicio9", "Equipos", "(NumSerie, Nombre, Facultad) VALUE('255Y', 'SchizophreniaInvest', 5);");
		insertData.insertData("UD18Ejercicio9", "Reserva", "(DNI, NumSerie, Comienzo, Fin) VALUE('39182736', 'A2E1', '2020-08-10 10:00:00', '2020-08-10 14:00:00');");
		insertData.insertData("UD18Ejercicio9", "Reserva", "(DNI, NumSerie, Comienzo, Fin) VALUE('39165336', 'UU32', '2020-08-10 14:01:00', '2020-08-10 20:00:00');");
		insertData.insertData("UD18Ejercicio9", "Reserva", "(DNI, NumSerie, Comienzo, Fin) VALUE('49718236', 'AAE2', '2020-08-11 11:00:00', '2020-08-12 11:00:00');");
		insertData.insertData("UD18Ejercicio9", "Reserva", "(DNI, NumSerie, Comienzo, Fin) VALUE('88122736', 'OPAR', '2020-08-12 11:01:00', '2020-08-13 14:00:00');");
		insertData.insertData("UD18Ejercicio9", "Reserva", "(DNI, NumSerie, Comienzo, Fin) VALUE('39184867', '255Y', '2020-08-13 14:01:00', '2020-08-15 20:00:00');");
		closeConnection.closeConnection();
	}
}
