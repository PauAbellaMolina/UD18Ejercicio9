package methods;

import java.sql.SQLException;
import java.sql.Statement;

import dto.Table;

public class insertData {
	public static void insertData(String nameDB, String tableName, String insertQuery) {
		try {
			String querydb = "USE " + nameDB + ";";
			Statement stdb = openConnection.connection.createStatement();
			stdb.executeUpdate(querydb);
			
			String query = "INSERT INTO " + tableName + insertQuery;
			Statement st = openConnection.connection.createStatement();
			st.executeUpdate(query);
			System.out.println("Data inserted!");
		}catch(SQLException e) {
			System.out.println("Error inserting the data: ");
			System.out.println(e.getMessage());
		}		
	}
}
