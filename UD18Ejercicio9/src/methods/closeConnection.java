package methods;

import java.sql.SQLException;

public class closeConnection {
	public static void closeConnection() {
		try {
			openConnection.connection.close();
			System.out.println("Disconnected!");
		}catch(SQLException e) {
			System.out.println("Error");
			System.out.println(e.getMessage());
		}
	}
}
