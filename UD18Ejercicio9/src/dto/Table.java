package dto;

public class Table {
	protected String nameDB;
	protected String tableName;
	
	public Table() {
		this.nameDB = "";
		this.tableName = "";
	}
	
	public Table(String nameDB, String tableName) {
		this.nameDB = nameDB;
		this.tableName = tableName;
	}

	public String getNameDB() {
		return nameDB;
	}
	
	public String getTableName() {
		return tableName;
	}
}